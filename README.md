# r_advanced_data_management_overview

## Objectif

**Compiler un fichier avec pour chaque département :**
- taux_hommes_vivants
- age_moyen_vivant
- age_moyen_deces
- taux_img
- taux_artisans_commerçants_chef_entreprises
- taux_profs_intermediaires
- taux_ouvriers
- taux_sans_act_pro
- taux_agriculteurs_exploitants
- taux_cadres
- taux_employés
- taux_retraités

Forker le répertoire Git + le Cloner <br>
A la fin du TD : pousser le script modifié et complété sur votre répertoire Git (ajouter @antoinelamer pour correction)

## Données brutes

| Fichier | Données | 
| -------- | -------- |
| BTT_TD_POP1B_2017.csv     | Population par ville     | 
| deces-2017.csv     | Décès     | 
| BTX_TD_IMG1A_2017.xlsx     | Population immigrées     | 
| TCRD_005.xlsx     | Statuts professionnels     | 

### Population par ville

| Fichier | Données | 
| -------- | -------- |
| nivgeo | Commune ou arrondissement |
| codgeo     | Code postal     | 
| sexe     | 1 = homme, 2 = femme     | 
| aged100     | âge jusqu'à 100 ans     | 
| nb     | Nombre d'habitants     |

Note : Lyon, Marseille et Paris ont à la fois la population globale et par arrondissement. Au cours du data management, nous retirerons la population par arrondissement.

### Décès

| Fichier | Données | 
| -------- | -------- |
| lieudeces     | Code postal     | 

### Population immigrée

| Fichier | Données | 
| -------- | -------- |
| codegeo     | Code postal     | 
| age400_immi1_sexe1 |  |

**age400_immi1_sexe1 :**

sexe1 : Hommes
sexe2 : Femmes
immi1 : Immigrés
immi2 : Non immigrés
age400 : Moins de 15 ans
age415 : 15 à 24 ans
age425 : 25 à 54 ans
age455 : 55 ans ou plus

## Exploration

**Fichiers brutes**
- type de fichier (excel, csv, txt)
- séparateur décimal
- fonction nécessaire pour ouvrir le fichier
- noms de colonnes exploitables
- unité statistique
- information (colonne) pertinente

**Dataframe optimal**
- unité statistique
- nouvelles variables

| Variable | Fichier source | Calcul |
| -------- | -------- | -------- |
| taux_hommes_vivants     |     | |
| age_moyen_vivant     |    | |
| age_moyen_deces     |      | |
| taux_img     |      | |
| taux_artisans_commerçants_chef_entreprises     |      ||
| taux_profs_intermediaires     |      | |
| taux_ouvriers     |      |  |
| taux_sans_act_pro     |      | |
| taux_agriculteurs_exploitants     |      | |
| taux_cadres     |      | |
| taux_employés |    | |
| taux_retraités     |      |  |

**Plan de data management**

## Data management

**Population (BTT_TD_POP1B_2017)**

Retirer les arrondissemements

**Décès (deces-2017.csv)**

**Population immigrée (BTX_TD_IMG1A_2017.xlsx)**

Retirer les valeurs manquantes pour faire la somme (na.rm = TRUE dans la fonction sum)

**Status professionnels (TCRD_005.xlsx)**

Renommer les colonnes
colnames(csp_raw) = c('departement', 'lib_departement', 'taux_agriculteurs_exploitants', 'taux_artisans_commerçants_chef_entreprises', 'taux_cadres', 'taux_profs_intermediaires', 'taux_employés', 'taux_ouvriers', 'taux_retraités', 'taux_sans_act_pro')
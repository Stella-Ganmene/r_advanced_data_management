---
title: "Data management"
author: "GANMENE TSAKOUE Stella Germaine"
date: "2023-10-22"
output: html_document
---

# Update the path with my own directory
```{r }
path_data = "C:\\Users\\Windows 10\\Desktop\\Data_Management\\r_advanced_data_management-main\\data"
#########################################
```
## Chargement des packages

##Population par ville


```{r}
pop_ville = read.csv2(file.path(path_data, "BTT_TD_POP1B_2017.csv"), sep = ";", dec = ".")
View(pop_ville)
```

#Attacher le fichier pop_ville

```{r}
attach(pop_ville)
```

#Retirer les arrondissemements
```{r}
library(dplyr)
RemoveColumns <- c("NIVGEO", "LIBGEO")
pop_ville_new <- pop_ville %>% select(- one_of(RemoveColumns))
```

#nouveau fichier de la population par ville
```{r}
View(pop_ville_new)
attach(pop_ville_new)
```
#verifier s'il existe des valeurs manquantes

#supprimer les doublons s'il existe
```{r}
pop_ville_new = unique(pop_ville_new)
pop_ville_new$SEXE <- as.factor(pop_ville_new$SEXE)
```

##Décès
```{r}
deces = read.csv2(file.path(path_data, "deces-2017.csv"))
View(deces)
```

```{r}
deces = unique(deces)
attach(deces)
```
## importation du fichier Population immigrée

```{r}
library(readxl)
pop_immi <- read_excel(file.path(path_data,"BTX_TD_IMG1A_2017.xlsx"))
View(pop_immi)
attach(pop_immi)
```


#Retirer les valeurs manquantes pour faire la somme
```{r}
pop_immi = unique(pop_immi)
```


## importer le fichier Status professionnels


```{r}
status <- read_excel(file.path(path_data,"TCRD_005.xlsx"))
View(status)
attach(status)
```

```{r}
doublons <- duplicated(status)
doublons
```
#renommer les colonnes du fichier statut
```{r}
library(dplyr)
status <- status %>% 
rename("departement"  = 1, "lib_departement" = 2, "taux_agriculteurs_exploitants" = 3, "taux_artisans_commerçants_chef_entreprises" = 4, "taux_cadres" = 5, "taux_profs_intermediaires" = 6, "taux_ouvriers" = 7, "taux_retraités" = 8, "taux_sans_act_pro" = 9)
```

```{r}
View(status)
```

```{r}
summary(status)
```




## population par ville

```{r}
pop_ville_new$departement <- substr(pop_ville_new$CODGEO, 1, 2)
View(pop_ville_new)
```


#taux_hommes_vivants
```{r}
Taux_hommes <- pop_ville_new %>%
  group_by(departement) %>%
  summarise(Total_Individus = n(),
            Nombre_Hommes = sum(SEXE == "1"),
            Taux_Hommes = round(Nombre_Hommes / Total_Individus * 100, digits = 2))
```


### Taux des hommes vivants dans un departement


```{r}
Taux_hommes <- subset(Taux_hommes, select = -c(Total_Individus, Nombre_Hommes))
```

```{r}
View(Taux_hommes)
```


##age moyen des vivants
```{r}
age_moyen <- pop_ville_new %>%
  group_by(departement) %>%
  summarise(Age_Moyen = mean(AGED100, na.rm = TRUE))
```

```{r}
View(age_moyen)
```


###age_moyen_deces
```{r}
summary(deces)
```
```{r}
deces$datenaiss <- as.Date(deces$datenaiss, format = "%Y%m%d")
deces$datedeces <- as.Date(deces$datedeces, format = "%Y%m%d")

```



# Calculez l'âge au décès en années

# ressortir la colonne des departements
```{r}
deces$departement <- substr(deces$lieunaiss, 1, 2)
View(deces)
```


```{r}
library(dplyr)


donnees_deces <- deces %>%
  mutate(âge = as.numeric(difftime(datedeces, datenaiss, units = "days")) / 365.25)
```

# Utilisez group_by() pour regrouper les données par département et calculez l'âge moyen
```{r}
age_moyen_deces <- donnees_deces %>%
  group_by(departement) %>%
  summarise(Âge_Moyen_Décès = mean(âge, na.rm = TRUE))
```

# Affichez le résultat
```{r}
View(age_moyen_deces)
```

# suppression des deux dernieres lignes
```{r}
age_moyen_deces <- head(age_moyen_deces, n = -2)
```

```{r}
str(age_moyen_deces)
```


#taux_img

#departement
```{r}
pop_immi$departement <- substr(pop_immi$CODGEO, 1, 2)
View(pop_immi)
```



```{r}
library(dplyr)

taux_immi <- pop_immi %>%
  group_by(departement) %>%
  summarise(Taux_Immigres = round(n() / sum(AGE400_IMMI1_SEXE1) * 100, digits = 2))

View(taux_immi)
#taux_immi = taux_immi[, -1]
```



#taux dans Status


# creation de la colonne departement
```{r}
status$departement <- substr(status$departement, 1, 2)
View(status)
```


#grouper les status par departement 

```{r}
library(dplyr)

status_final <- status %>%
  select(-lib_departement) %>%
  group_by(departement) %>%
  summarise_all(~ sum(., na.rm = TRUE))


```


```{r}
View(status_final)
```

## suppression des deux dernieres lignes
```{r}
status_final <- head(status_final, n = -2)
```

```{r}
status_final
```




#### Fichiers final


```{r}
str(Taux_hommes)
```

```{r}
str(age_moyen_deces)
```
```{r}
str(age_moyen)
```

```{r}
str(status_groupes_fin)
```





```{r}
list_data = list(Taux_hommes, age_moyen,age_moyen_deces ,status_final)

```

```{r}
library(dplyr)
library(purrr)
```


```{r}
resultat_final <- reduce(list_data, full_join, by = "departement")

```

#suppression de la derniere ligne de notre fichier final

```{r}
resultat_final <- head(resultat_final, n = -1)
```



```{r}
View(resultat_final)
```

